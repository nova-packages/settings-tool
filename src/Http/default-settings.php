<?php

return [
    'general' => [
        'site-title' => [
            'component' => 'text-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
        'site-sub-title' => [
            'component' => 'text-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
        'site-url' => [
            'component' => 'text-field',
            'value' => url('/'),
            'is_visible' => true,
            'is_readonly' => true,
        ],
        'site-logo' => [
            'component' => 'image-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
    ],
    'analytics' => [
        'ga-tag' => [
            'component' => 'text-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
    ],
    'email' => [
        'from-name' => [
            'component' => 'text-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
        'from-email' => [
            'component' => 'text-field',
            'is_visible' => true,
            'is_readonly' => false,
        ],
    ],
    'advanced' => [

    ]
];
