<router-link tag="h3" :to="{name: 'settings'}" class="cursor-pointer flex items-center font-normal dim text-white mb-6 text-base no-underline">
    <i class="material-icons md-18 mr-3">settings</i>
    <span class="sidebar-label">
        Instellingen
    </span>
</router-link>
