<?php

namespace DesignCode\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'scope', 'name', 'value', 'component', 'is_visible', 'is_readonly'
    ];

    protected $casts = [
        'is_visible' => 'boolean',
        'is_readonly' => 'boolean',
    ];

    public function label()
    {
        return "{$this->scope}.{$this->name}";
    }
}
