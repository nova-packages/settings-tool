<?php

namespace DesignCode\Settings\Http\Controllers;

use App\Http\Controllers\Controller;
use DesignCode\Settings\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
        $data = [];
        foreach ($settings as $setting) {
            if ($setting->is_visible) {
                $data[$setting->scope][] = [
                    'component' => $setting->component,
                    'prefixComponent' => true,
                    'indexName' => $setting->label(),
                    'name' => $setting->label(),
                    'attribute' => $setting->label(),
                    'value' => $setting->value,
                    'panel' => null,
                    'sortable' => false,
                    'nullable' => false,
                    'readonly' => $setting->is_readonly,
                    'textAlign' => 'left'
                ];
            }
        }
        return $data;
    }

    public function update(Request $request)
    {
        $jsonData = json_decode($request->get('fields'), JSON_OBJECT_AS_ARRAY);

        foreach($jsonData as $scope => $items) {
            foreach ($items as $item) {
                Setting::where('scope', $scope)->where('name', str_replace("{$scope}.", '', $item['name']))->update([
                    'value' => $item['value']
                ]);
            }
        }
        $files = $request->allFiles();
        /**
         * @var  $key
         * @var UploadedFile $file
         */
        foreach ($files as $key => $file) {
            $fileName = str_replace(' ', '-', $file->getClientOriginalName());
            $file->move(storage_path('app/public/images'), $fileName);
            $keyData = explode('_', $key);
            Setting::where('scope', $keyData[0])->where('name', $keyData[1])->update([
                'value' => url('storage/images/' . $fileName)
            ]);
//            $file->move()
        }
        return $this->index();
    }

    public function install() : void
    {
        $defaultSettings = include __DIR__ . '/../default-settings.php';
        foreach ($defaultSettings as $scope => $items) {
            foreach ($items as $field => $data) {
                $data = [
                        'scope' => $scope,
                        'name' => $field,
                    ] + $data;
                Setting::updateOrCreate([
                    'scope' => $scope,
                    'name' => $field,
                ], $data);
            }
        }
    }
}
